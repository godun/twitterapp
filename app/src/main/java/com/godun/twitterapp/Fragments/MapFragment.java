package com.godun.twitterapp.Fragments;


import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
//import android.app.Fragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.godun.twitterapp.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
//import static com.godun.twitterapp.R.id.map;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapFragment extends Fragment implements OnMapReadyCallback {
    private static LayoutInflater _inflater;
    private String TAG = getClass().getSimpleName();
    private GoogleMap mMap;
    private MapView mMapView;

    public MapFragment() {
        // Required empty public constructor
    }

    private void markerMaker(LatLng latLng, String title, String snippet, BitmapDescriptor icon, int tag) {
        mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title(title)
                .snippet(snippet)
                .icon(icon)).setTag(tag);
    }

    private void openDialog() {
        final String url = "https://en.wikipedia.org/wiki/Kropyvnytskyi";

        final AlertDialog aboutDialog = new AlertDialog.Builder(
                _inflater.getContext()).setMessage("Article about Kropyvnytskyi\n" +
                "From Wikipedia, the free encyclopedia")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(url));
                        startActivity(i);
                    }
                }).create();

        aboutDialog.show();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        _inflater = inflater;
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);
        mMapView = (MapView) rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        mMapView.getMapAsync(this);
        return rootView;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMinZoomPreference(7.0f);
        mMap.setMaxZoomPreference(18.0f);
        UiSettings mUiSettings;
        mUiSettings = mMap.getUiSettings();
        mUiSettings.setCompassEnabled(true);
        mUiSettings.setZoomControlsEnabled(true);
        LatLng krop_center = new LatLng(48.510309, 32.266744);
        LatLng school = new LatLng(48.508738, 32.266701);
        LatLng depot = new LatLng(48.507403, 32.264239);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(krop_center, 15));

        int[] tags = {0, 1, 2};

        markerMaker(krop_center, "Kropivnitsky", "Population: 231 181", BitmapDescriptorFactory.defaultMarker(), tags[0]);
        markerMaker(school, "School №6", "Taras Karpy, 63", BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker_white_48dp), tags[1]);
        markerMaker(depot, "Depot", "Shopping center", BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE), tags[2]);

        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                String url = "";
                switch ((int) marker.getTag()) {
                    case 0:
                        url = null;
                        openDialog();
                        break;
                    case 1:
                        url = "http://www.school6kr.org.ua/";
                        break;
                    case 2:
                        url = "http://www.ddgroup.com.ua/ru/objects/shoppings/260";

                        break;
                }

                if (url != null) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
}

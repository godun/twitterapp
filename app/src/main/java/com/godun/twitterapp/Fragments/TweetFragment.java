package com.godun.twitterapp.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.godun.twitterapp.R;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.tweetui.TweetUtils;
import com.twitter.sdk.android.tweetui.TweetView;

/**
 * A simple {@link Fragment} subclass.
 */
public class TweetFragment extends Fragment {

    private static final String MY_ID_KEY = "tweet_id";
    private static LayoutInflater _inflater;
    private long tweet_id;

    public TweetFragment() {
        // Required empty public constructor
    }

    public static TweetFragment newInstance(long id) {
        TweetFragment f = new TweetFragment();
        Bundle args = new Bundle();
        args.putLong(MY_ID_KEY, id);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tweet_id = getArguments() != null ? getArguments().getLong(MY_ID_KEY) : 510908133917487104L;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        _inflater = inflater;
        return inflater.inflate(R.layout.fragment_tweet, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        View view = getView();
        if (view != null) {
            final FrameLayout myLayout
                    = (FrameLayout) view.findViewById(R.id.my_tweet_layout);
            final long tweetId = this.tweet_id;
            TweetUtils.loadTweet(tweetId, new Callback<Tweet>() {
                @Override
                public void success(Result<Tweet> result) {
                    myLayout.addView(new TweetView(_inflater.getContext(), result.data));
                }

                @Override
                public void failure(TwitterException exception) {
                }
            });
        }
    }

}

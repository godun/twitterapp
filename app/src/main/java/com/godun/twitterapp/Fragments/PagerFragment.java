package com.godun.twitterapp.Fragments;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.godun.twitterapp.MyAdapter;
import com.godun.twitterapp.R;
import com.godun.twitterapp.VerticalViewPager;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.core.services.AccountService;
import com.twitter.sdk.android.core.services.StatusesService;

import java.util.List;

import retrofit2.Call;

/**
 * A simple {@link Fragment} subclass.
 */
public class PagerFragment extends Fragment {
    private String TAG = getClass().getSimpleName();
    private int NUMBER_OF_PAGES;
    private MyAdapter mAdapter;
    private VerticalViewPager mPager;
    private String user_name;
    private static final String MY_USER_NAME_KEY = "user_name";


    private static LayoutInflater _inflater;

    public static PagerFragment newInstance(String name) {
        PagerFragment f = new PagerFragment();
        Bundle args = new Bundle();
        args.putString(MY_USER_NAME_KEY, name);
        f.setArguments(args);
        return f;
    }

    public PagerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user_name = getArguments() != null ? getArguments().getString(MY_USER_NAME_KEY) : "";
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        _inflater = inflater;
        return inflater.inflate(R.layout.fragment_pager, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        View view = getView();
        if (view != null) {
            TwitterApiClient twitterApiClient = TwitterCore.getInstance().getApiClient();
            StatusesService statusesService = twitterApiClient.getStatusesService();
            Call<List<Tweet>> tweets = statusesService.userTimeline(null, user_name, null, null, null, null, null, null, null);
            tweets.enqueue(new Callback<List<Tweet>>() {
                @Override
                public void success(Result<List<Tweet>> result) {
                    Log.d(TAG, "success: size: " + result.data.size());
//                    result.data.get(0).;
                    NUMBER_OF_PAGES = result.data.size();
                    mAdapter = new MyAdapter(getChildFragmentManager(), NUMBER_OF_PAGES, result.data);
                    mPager = view.findViewById(R.id.viewpager);
                    mPager.setAdapter(mAdapter);
                }

                @Override
                public void failure(TwitterException exception) {

                }
            });

            FloatingActionButton fab_up = (FloatingActionButton) view.findViewById(R.id.fab_up);
            fab_up.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    Toast.makeText(_inflater.getContext(), "fab_up", Toast.LENGTH_SHORT).show();
                    mPager.setCurrentItem(mPager.getCurrentItem() - 1, true); //getItem(-1) for previous
                }
            });

            FloatingActionButton fab_down = (FloatingActionButton) view.findViewById(R.id.fab_down);
            fab_down.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    Toast.makeText(_inflater.getContext(), "fab_down", Toast.LENGTH_SHORT).show();
                    mPager.setCurrentItem(mPager.getCurrentItem() + 1, true); //getItem(-1) for previous
                }
            });

        }

    }
}

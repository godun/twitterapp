package com.godun.twitterapp.Fragments;


import android.os.Bundle;
//import android.app.Fragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.godun.twitterapp.R;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.core.services.AccountService;
import com.twitter.sdk.android.core.services.FavoriteService;
import com.twitter.sdk.android.core.services.ListService;
import com.twitter.sdk.android.core.services.SearchService;
import com.twitter.sdk.android.core.services.StatusesService;
import com.twitter.sdk.android.tweetui.FixedTweetTimeline;
import com.twitter.sdk.android.tweetui.SearchTimeline;
import com.twitter.sdk.android.tweetui.TweetTimelineRecyclerViewAdapter;
import com.twitter.sdk.android.tweetui.TweetUtils;
import com.twitter.sdk.android.tweetui.TweetView;
import com.twitter.sdk.android.tweetui.UserTimeline;

import retrofit2.Call;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {
    private static LayoutInflater _inflater;
    private String TAG = getClass().getSimpleName();
    private String user_name;
    private static final String MY_USER_NAME_KEY = "user_name";


    public static HomeFragment newInstance(String name){
        HomeFragment f = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(MY_USER_NAME_KEY, name);
        f.setArguments(args);
        return f;
    }

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user_name = getArguments() != null ? getArguments().getString(MY_USER_NAME_KEY) : "";
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        _inflater = inflater;
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        View view = getView();
        if (view != null) {
            if (user_name != null) {
                RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.rv_twitter_home);
                recyclerView.setLayoutManager(new LinearLayoutManager(_inflater.getContext()));
                UserTimeline userTimeline = new UserTimeline.Builder().screenName(user_name).build();

                final TweetTimelineRecyclerViewAdapter adapter =
                        new TweetTimelineRecyclerViewAdapter.Builder(_inflater.getContext())
                                .setTimeline(userTimeline)
                                .setViewStyle(R.style.tw__TweetLightWithActionsStyle)
                                .build();

                recyclerView.setAdapter(adapter);
            }
        }
    }
}

package com.godun.twitterapp.Fragments;


import android.os.Bundle;
//import android.app.Fragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.godun.twitterapp.R;
import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.models.User;
import com.twitter.sdk.android.core.services.AccountService;

import retrofit2.Call;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {
    private static LayoutInflater _inflater;
    private String TAG = getClass().getSimpleName();
    private ImageView iv_profile_banner, iv_profile_image;
    private TextView tv_full_name, tv_user_screen_name, tv_description, tv_email, tv_counter;

    public ProfileFragment() {
        // Required empty public constructor
    }

    private void findView(View view) {
        iv_profile_banner = (ImageView) view.findViewById(R.id.iv_profile_banner);
        iv_profile_image = (ImageView) view.findViewById(R.id.iv_profile_image);
        tv_full_name = (TextView) view.findViewById(R.id.tv_full_name);
        tv_user_screen_name = (TextView) view.findViewById(R.id.tv_user_screen_name);
        tv_description = (TextView) view.findViewById(R.id.tv_description);
        tv_email = (TextView) view.findViewById(R.id.tv_email);
        tv_counter = (TextView) view.findViewById(R.id.tv_counter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        _inflater = inflater;
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        View view = getView();
        if (view != null) {
            findView(view);

            TwitterApiClient twitterApiClient = TwitterCore.getInstance().getApiClient();
            AccountService accountService = twitterApiClient.getAccountService();
            Call<User> call = accountService.verifyCredentials(true, true, true);
            call.enqueue(new Callback<User>() {
                @Override
                public void success(Result<User> result) {
                    //here we go User details
                    try {
                        User user = result.data;
                        String fullname = user.name;
                        String userEmail = user.email;
                        String userScreenName = user.screenName;
                        String profileImageUrl = user.profileImageUrl;
                        String profileBannerUrl = user.profileBannerUrl;
                        String description = user.description;
                        int friendsCount = user.friendsCount;
                        int statusesCount = user.statusesCount;
                        profileImageUrl = profileImageUrl.replaceAll("_normal", "");

                        Picasso.with(_inflater.getContext())
                                .load(profileImageUrl)
                                .resizeDimen(R.dimen.resize_profile_image, R.dimen.resize_profile_image)
                                .into(iv_profile_image);
                        Picasso.with(_inflater.getContext())
                                .load(profileBannerUrl)
//                                .fit()
//                                .centerCrop()
//                                .resize(600, 200)
//                                .centerCrop()
                                .into(iv_profile_banner);
                        tv_full_name.setText(fullname);
                        tv_user_screen_name.setText(getString(R.string.profile_user_screen_name, userScreenName));
                        if (!description.isEmpty()) {
                            tv_description.setText(description);
                            tv_description.setVisibility(View.VISIBLE);
                        } else {
                            tv_description.setVisibility(View.GONE);
                        }

                        if (userEmail != null) {
                            tv_email.setText(userEmail);
                            tv_email.setVisibility(View.VISIBLE);
                        } else {
                            tv_email.setVisibility(View.GONE);
                        }

                        tv_counter.setText(String.format(getResources().getString(R.string.profile_counter_labels), statusesCount, friendsCount));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void failure(TwitterException exception) {
                }
            });
        }
    }
}

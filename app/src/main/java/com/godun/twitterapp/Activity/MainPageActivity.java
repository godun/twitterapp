package com.godun.twitterapp.Activity;

//import android.app.FragmentManager;
//import android.app.FragmentTransaction;
//import android.app.FragmentTransaction;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;

import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.godun.twitterapp.Fragments.EmptyFragment;
import com.godun.twitterapp.Fragments.HomeFragment;
import com.godun.twitterapp.Fragments.MapFragment;
import com.godun.twitterapp.Fragments.PagerFragment;
import com.godun.twitterapp.Fragments.ProfileFragment;
import com.godun.twitterapp.R;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterCore;

public class MainPageActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private String TAG = getClass().getSimpleName();
    public static String EXTRA_USER_NAME = "user_name";
    public static String EXTRA_USER_ID = "user_id";
    private int currentPos = 0;
    private Toolbar toolbar;
    private TextView mTitle;
    private ImageView toolbar_image;
    private String user_name;
    private long userId;
    private boolean switcher = false;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    selectItem(0);
                    return true;
                case R.id.navigation_profile:
                    selectItem(1);
                    return true;
                case R.id.navigation_map:
                    selectItem(2);
                    return true;
                case R.id.navigation_empty:
                    selectItem(3);
                    return true;
            }
            return false;
        }
    };

    public void selectItem(int position) {
        currentPos = position;
        Fragment fragment;
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        switch (position) {
            case 0:
                fragment = HomeFragment.newInstance(user_name);
                toolbar_image.setVisibility(View.VISIBLE);
                toolbar_image.setImageResource(R.drawable.ic_file_document_white_24dp);
                break;
            case 1:
                fragment = new ProfileFragment();
                toolbar_image.setVisibility(View.GONE);
                break;
            case 2:
                fragment = new MapFragment();
                toolbar_image.setVisibility(View.GONE);
                break;
            case 3:
                fragment = new EmptyFragment();
                toolbar_image.setVisibility(View.GONE);
                break;
            case 4:
                fragment = PagerFragment.newInstance(user_name);
                toolbar_image.setVisibility(View.VISIBLE);
                toolbar_image.setImageResource(R.drawable.ic_format_list_bulleted_white_24dp);
                break;
            default:
                fragment = new HomeFragment();
                toolbar_image.setVisibility(View.VISIBLE);
        }

        ft.replace(R.id.content_frame, fragment, "visible_fragment");
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commit();
        setActionBarTitle(position);
    }

    private void setActionBarTitle(int pos) {
        Log.d(TAG, "setActionBarTitle: pos: " + pos);
        String title;
        switch (pos) {
            case 0:
                title = getString(R.string.title_home);
                break;
            case 1:
                title = getString(R.string.title_profile);
                break;
            case 2:
                title = getString(R.string.title_map);
                break;
            case 3:
                title = getString(R.string.title_empty);
                break;
            case 4:
                title = getString(R.string.title_home);
                break;
            default:
                title = getString(R.string.title_home);
        }
        Log.d(TAG, "setActionBarTitle: title: " + title);
        mTitle.setText(title);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_page);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar_image = (ImageView) toolbar.findViewById(R.id.toolbar_image);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        user_name = getIntent().getStringExtra(EXTRA_USER_NAME);
        userId = getIntent().getLongExtra(EXTRA_USER_ID, 0);

        if (savedInstanceState != null) {
            currentPos = savedInstanceState.getInt("position");
            switcher = savedInstanceState.getBoolean("switcher");
            selectItem(currentPos);
        } else {
            selectItem(0);
        }

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        toolbar_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (switcher) {
                    selectItem(0);
                    switcher = !switcher;
                } else {
                    selectItem(4);
                    switcher = !switcher;
                }
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                mTitle.setVisibility(View.GONE);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                mTitle.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_help) {
            String url = "https://dev.twitter.com/twitterkit/android/overview";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        } else if (id == R.id.nav_log_out) {
            TwitterCore.getInstance().getSessionManager().clearActiveSession();
            Intent intent = new Intent(MainPageActivity.this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_skills) {
            String url = "https://bitbucket.org/godun/medlux";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.END);
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (id == R.id.action_settings) {
            drawer.openDrawer(GravityCompat.END);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("position", currentPos);
        outState.putBoolean("switcher", switcher);
    }
}

package com.godun.twitterapp;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.godun.twitterapp.Fragments.TweetFragment;
import com.twitter.sdk.android.core.models.Tweet;

import java.util.List;

/**
 * Created by godun on 01.02.2018.
 */

public class MyAdapter extends FragmentPagerAdapter {
    private int NUMBER_OF_PAGES;
    private List<Tweet> tweets;

    public MyAdapter(FragmentManager fm, int pages, List<Tweet> tweets) {
        super(fm);
        NUMBER_OF_PAGES = pages;
        this.tweets = tweets;
    }

    @Override
    public int getCount() {
        return NUMBER_OF_PAGES;
    }

    @Override
    public Fragment getItem(int position) {
        return TweetFragment.newInstance(tweets.get(position).getId());
    }


}


